package de.grogra.graph.object.sg.impl.io;

import de.grogra.graph.object.sg.impl.SecGraphRef;
import de.grogra.pf.ui.Context;

public class RuleGraphGenerator {

	public static Object create (Context ctx) {
		SecGraphRef sgr = new SecGraphRef("rule");
		sgr.getQueryRoot(true);
		sgr.getProductionRoot(true);
		sgr.write();
//		return ((SecGraphImpl)sgr.resolve()).getProvider();
		return null;		
	}
}
