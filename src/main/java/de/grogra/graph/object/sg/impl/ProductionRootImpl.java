package de.grogra.graph.object.sg.impl;

import de.grogra.graph.impl.Node;
import de.grogra.graph.object.sg.ProductionRoot;

public class ProductionRootImpl extends Node implements ProductionRoot {
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new ProductionRootImpl ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ProductionRootImpl ();
	}

//enh:end
	
	@Override
	public int getSymbolColor ()
	{
		return 0x00FF6600;
	}
}
