package de.grogra.graph.object.sg.impl.io;

import java.io.IOException;

import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.GraphXMLSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.StreamAdapter;
import de.grogra.pf.ui.Workbench;
import de.grogra.graph.impl.Node;
import de.grogra.graph.object.sg.impl.SecGraphImpl;
import de.grogra.persistence.PersistenceBindings;

/**
 * This filter is used when a project that contains SecGraph object is open. The objects
 * are loaded using this filter.
 */
public class SecGraphFilter extends FilterBase implements ObjectSource {

	public static final IOFlavor FLAVOR = IOFlavor.valueOf (SecGraphImpl.class);
	
	public SecGraphFilter(FilterItem item, FilterSource source) {
		super (item, source);
		setFlavor (FLAVOR);
	}
	
	@Override
	public Object getObject() throws IOException {
		SecGraphImpl sg = new SecGraphImpl(Workbench.current());
		StreamAdapter a = new StreamAdapter(source, new IOFlavor(GraphXMLSource.MIME_TYPE, IOFlavor.SAX, null));
		
		XMLRootlessGraphReader reader = new XMLRootlessGraphReader(
				new PersistenceBindings(getRegistry(), getRegistry()), sg.getGraph(), new Node[] {sg.getRootNode()});

		//XMLGraphReader reader = new XMLGraphReader(getRegistry().getProjectGraph().getBindings(), sg.getGraph());
		try {
			a.parse(reader, null, null, null, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(String key : reader.getRoots().keySet()) {
			sg.getGraph().setRoot(key, (Node)reader.getRoots().get(key));
		}
		return sg;
	}
	
	
}
