package de.grogra.graph.object.sg.impl;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.graph.object.sg.SecGraph;
import de.grogra.xl.util.IntList;
import de.grogra.xl.util.ObjectList;

public class SecGraphInitialProducer {

	SecGraph sg;
	Node start;

	public SecGraphInitialProducer(Node start) {
		this.start = start;
	
	}

	public SecGraphInitialProducer producer$begin() {
		return this;
	}

	public SecGraphInitialProducer producer$end() {
		return this;
	}

	public SecGraphInitialProducer operator$space(Node node) {
		addNode(node);
		return this;
	}

	public SecGraphInitialProducer producer$push() {
		push();
		return this;
	}

	public SecGraphInitialProducer producer$pop(Object oldProducer) {
		pop();
		return this;
	}

	public SecGraphInitialProducer operator$slashArrow(Node n) {
		addNodeRight(n, Graph.REFINEMENT_EDGE);
		return this;
	}

	public SecGraphInitialProducer operator$slashLeftArrow(Node n) {
		addNodeLeft(n, Graph.REFINEMENT_EDGE);
		return this;
	}

	public SecGraphInitialProducer operator$slashLeftRightArrow(Node n) {
		addNodeRightLeft(n, Graph.REFINEMENT_EDGE);
		return this;
	}

	public SecGraphInitialProducer operator$plusArrow(Node n) {
		addNodeRight(n, Graph.BRANCH_EDGE);
		return this;
	}

	public SecGraphInitialProducer operator$plusLeftArrow(Node n) {
		addNodeLeft(n, Graph.BRANCH_EDGE);

		return this;
	}

	public SecGraphInitialProducer operator$plusLeftRightArrow(Node n) {
		addNodeRightLeft(n, Graph.BRANCH_EDGE);

		return this;
	}

	public SecGraphInitialProducer operator$arrow(Node n, int edge) {
		addNodeRight(n, edge);
		return this;
	}

	public SecGraphInitialProducer operator$leftArrow(Node n, int edge) {
		addNodeLeft(n, edge);
		return this;
	}

	public SecGraphInitialProducer operator$xLeftRightArrow(Node n, int edge) {
		addNodeRightLeft(n, edge);
		return this;
	}

	public SecGraphInitialProducer operator$gt(Node n) {
		addNodeRight(n, Graph.SUCCESSOR_EDGE);

		return this;
	}

	public SecGraphInitialProducer operator$lt(Node n) {
		addNodeLeft(n, Graph.SUCCESSOR_EDGE);

		return this;
	}

	public SecGraphInitialProducer operator$leftRightArrow(Node n) {
		addNodeRightLeft(n, Graph.SUCCESSOR_EDGE);

		return this;
	}

	public SecGraphInitialProducer producer$separate() {
		nextNew = true;
		return this;
	}
	public Node producer$getRoot() {
		return start;
	}

	private int nextEdgeByte = Graph.SUCCESSOR_EDGE;
	private ObjectList nStack = new ObjectList();
	private IntList eStack = new IntList();

	private Node last;
	private boolean nextNew = false;

	public void addNode(Node n) {
		if (nextNew) {
			last = n;
			nextNew = false;
		} else {
			if (last == null) {
				last = start;
			}
			last.addEdgeBitsTo(n, nextEdgeByte, null);
			last = n;
			nextEdgeByte = Graph.SUCCESSOR_EDGE;
		}
	}

	public void addNodeRight(Node n, int edgeBit) {
		if (last == null) {
			last = start;
		}
		last.addEdgeBitsTo(n, edgeBit, null);
		last = n;
	}

	public void addNodeLeft(Node n, int edgeBit) {
		if (last == null) {
			last = start;
		}
		n.addEdgeBitsTo(last, edgeBit, null);
		last = n;
	}

	public void addNodeRightLeft(Node n, int edgeBit) {
		if (last == null) {
			last = start;
		}
		last.addEdgeBitsTo(n, edgeBit, null);
		n.addEdgeBitsTo(last, edgeBit, null);

		last = n;
	}

	public void setNextEdgeByte(int edge) {
		nextEdgeByte = edge;
	}

	public void push() {
		nStack.push(last);
		eStack.push(nextEdgeByte);
		nextEdgeByte = Graph.BRANCH_EDGE;
	}

	public void pop() {
		last = (Node) nStack.pop();
		nextEdgeByte = eStack.pop();
	}
}
