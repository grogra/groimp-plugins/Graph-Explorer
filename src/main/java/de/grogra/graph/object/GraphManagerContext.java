package de.grogra.graph.object;

import de.grogra.graph.impl.GraphManager;

public interface GraphManagerContext extends GraphContext{

	public GraphManager getGraph();
}
