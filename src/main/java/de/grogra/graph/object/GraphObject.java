package de.grogra.graph.object;

import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.Context;

public interface GraphObject extends RegistryContext, GraphManagerContext,de.grogra.imp.objects.ProducingNode{
	/**
	 * Return the root node of the GraphObject, i.e. the root of the object loaded
	 * @return a Node 
	 */
	public Node getRootNode();
	/**
	 * Set the rootnode of a GraphObject object. 
	 * @param root
	 * @return
	 */
	public void setRootNode(Node root);
	
	/**
	 * clone the graph of the rootnode and returns it
	 * @return
	 */
	public Node cloneGraph() throws CloneNotSupportedException;
	
	
	/**
	 * Override the file GraphObject in the project with its current version.
	 * Required on GraphObject created in RGG to be saved
	 */
	public void write();
	
	/**
	 * If the GraphObject uses some modules defined in the project, reload forces 
	 * the modules in the GraphObject to be relinked after compilation
	 */
	public void reload();
	
	/**
	 * Gives an array of node types and number contained in the GraphObject
	 * @return
	 */
	public Object describes();
	
	/**
	 * Return the graph manager of the GraphObject. It is different from the graph manager
	 * of the project & workbench. 
	 * @return
	 */
	public GraphManager getGraph();
	
	/**
	 * Return a node of the given name
	 * @param key: the name of the type of node
	 */
	public Node getNode(String key);
	
	/**
	 * Resolves the real object if a reference is used
	 * If the real object is used the function returns it self
	 * 
	 * @return
	 */
	
	public GraphObject resolve();
	
	/**
	 * Opens the 3d view of the current graph
	 * 
	 * @param ctx
	 */
	public void view3d(Context ctx);
	
	/**
	 * Opens the 2d view of the current graph
	 * 
	 * @param ctx
	 */
	
	
	public void view2d(Context ctx);

	/**
	 * Gives the name if one exists
	 * @return
	 */
	
	public String getName();

}
