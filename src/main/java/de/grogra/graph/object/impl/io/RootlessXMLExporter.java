package de.grogra.graph.object.impl.io;

import java.io.File;
import java.io.IOException;

import de.grogra.graph.impl.GraphManager;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.GraphXMLSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.StreamAdapter;

public class RootlessXMLExporter extends SceneGraphExport implements FileWriterSource {

	public RootlessXMLExporter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(item.getOutputFlavor()); // IOflavor retrieved from filter item each time
	}

	@Override
	public void write(File out) throws IOException {
		GraphXMLSource gxs = new GraphXMLSource((GraphManager) getView().getGraph(), getRegistry(), null);
		StreamAdapter sad = new StreamAdapter(gxs,new IOFlavor(GraphXMLSource.MIME_TYPE, IOFlavor.OUTPUT_STREAM, null));
		sad.write(out);
	}

	@Override
	protected SceneTree createSceneTree(View3D scene) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void beginGroup(InnerNode group) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void endGroup(InnerNode group) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
