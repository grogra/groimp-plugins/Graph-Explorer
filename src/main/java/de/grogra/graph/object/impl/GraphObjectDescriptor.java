package de.grogra.graph.object.impl;


import de.grogra.graph.Graph;
import de.grogra.graph.object.GraphObject;
import de.grogra.imp.GraphDescriptor;
import de.grogra.imp.View;

public class GraphObjectDescriptor extends GraphDescriptor {

	//enh:sco

	public GraphObjectDescriptor(GraphObject graphObject) {
		this.graphObject=graphObject;
	}
	public GraphObjectDescriptor() {

	}


	@Override
	public Graph getGraph(View view) {
		if(graphObject ==null) {
			return view.getWorkbench ().getRegistry ().getProjectGraph ();
		}
		return graphObject.getGraph();

	}
/*
	@Override
	public Graph getGraph (View view)
	{
		if(secGraph ==null) {
			return view.getWorkbench ().getRegistry ().getProjectGraph();
		}
		return new GraphFilter (secGraph.getGraph())
		{
			@Override
			public Object getRoot (String key)
			{
				
				return secGraph.getGraph().getRoot(SecGraph.PRODUCTION_ROOT);//.getNode();
			}

			public void accept (Object startNode, final Visitor visitor,
								ArrayPath placeInPath)
			{
				accept (startNode, visitor, placeInPath, false);
			}
		};
	}
*/
	
	protected GraphObject graphObject;
	//enh:field getter, setter
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field graphObject$FIELD;

	public static class Type extends GraphDescriptor.Type
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (GraphObjectDescriptor representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, GraphDescriptor.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = GraphDescriptor.Type.FIELD_COUNT;
		protected static final int FIELD_COUNT = GraphDescriptor.Type.FIELD_COUNT + 1;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((GraphObjectDescriptor) o).graphObject = (GraphObject) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((GraphObjectDescriptor) o).getGraphObject ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new GraphObjectDescriptor ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (GraphObjectDescriptor.class);
		graphObject$FIELD = Type._addManagedField ($TYPE, "graphObject", Type.Field.PROTECTED  | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (GraphObject.class), null, Type.SUPER_FIELD_COUNT + 0);
		$TYPE.validate ();
	}

	public GraphObject getGraphObject ()
	{
		return graphObject;
	}

	public void setGraphObject (GraphObject value)
	{
		graphObject$FIELD.setObject (this, value);
	}

//enh:end

}
