package de.grogra.graph.object.impl;

import javax.vecmath.Matrix4d;

import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Node;
import de.grogra.graph.object.GraphObject;
import de.grogra.imp3d.objects.Instance3D;

public class GraphInstance extends Instance3D {

	private static final long serialVersionUID = -3482557966276518320L;

	private GraphObject sg;
	//enh:field getter setter
	
	
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field sg$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (GraphInstance.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((GraphInstance) o).sg = (GraphObject) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((GraphInstance) o).getSg ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new GraphInstance ());
		$TYPE.addManagedField (sg$FIELD = new _Field ("sg", _Field.PRIVATE  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (GraphObject.class), null, 0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new GraphInstance ();
	}

	public GraphObject getSg ()
	{
		return sg;
	}

	public void setSg (GraphObject value)
	{
		sg$FIELD.setObject (this, value);
	}

//enh:end
	
	public GraphInstance() {
		super();
	}
	
	public GraphInstance(GraphObject sg) {
		super();
		this.sg = sg;
	}
	
	@Override
	protected Node getInstanceRootSetAttributes (GraphState gs)
	{

		sg.resolve();
		if(sg.resolve().getRootNode()==null) {
			return null;//super.getInstanceRootSetAttributes (gs);
		}
		return sg.getRootNode();
	}
	
	@Override
	public void postTransform (Object object, boolean asNode, Matrix4d in, Matrix4d out, Matrix4d pre,
			   GraphState gs){
		if(sg.getRootNode()!=null) {
			gs = sg.getGraph().getMainState();
			super.postTransform(object, asNode, in, out, pre, gs);
		}
	}
	
}
