package de.grogra.graph.object.impl;

import java.awt.datatransfer.Transferable;

import de.grogra.graph.object.GraphObject;
import de.grogra.graph.object.sg.SecGraph;
import de.grogra.graph.object.sg.impl.SecGraphImpl;
import de.grogra.graph.object.sg.impl.exception.NoRuleGraphException;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.edit.Selection;
import de.grogra.pf.ui.util.ButtonWidget;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.rgg.model.Runtime;

public class GraphObjectSelection implements Selection{
	private GraphObject sg;
	private Context ctx;
	public GraphObjectSelection(GraphObject sg,Context ctx) {
		this.sg= sg;
		this.ctx=ctx;
	}
	
	@Override
	public Object getDescription(String type) {
		// TODO Auto-generated method stub
		return sg.getName();
	}

	@Override
	public int getCapabilities() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ComponentWrapper createPropertyEditorComponent() {
		UIToolkit ui= UIToolkit.get(getContext());
		Object container =  ui.createContainer(10,1,0);
		Object containerRow =  ui.createContainer(1,3,0);
	
		Object name= ui.createLabel(sg.getName(),0);
		ui.addComponent(container, name, null);
		Object view3d = ui.createButton("view3d", 0, new Command() {

			@Override
			public String getCommandName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void run(Object info, Context context) {
				sg.view3d(context);
				
			}
			
			
		}, getContext());

		Object view2d = ui.createButton("view2d", 0, new Command() {

			@Override
			public String getCommandName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void run(Object info, Context context) {
				sg.view2d(context);
				
			}	
		}, getContext());
		
		ui.addComponent(containerRow, view2d, null);
		ui.addComponent(containerRow, view3d, null);
		
//	Object export = ui.createButton("export", 0, new Command() {
//
//			@Override
//			public String getCommandName() {
//				return "exportSG";
//			}
//
//			@Override
//			public void run(Object info, Context context) {
//				GraphObjectDescriptor sgd = new GraphObjectDescriptor(sg);
//				View3D view3d = (View3D) View.create(new View3D(), context.getWorkbench(), new StringMap());
//				view3d.setGraph(sgd);
//				View view = (View) view3d.getPanel();
//				context.getWorkbench().setProperty(Workbench.EXPORT_VISIBLE_LAYER, false);
//				context.getWorkbench().export(new ObjectSourceImpl (view, "view", view.getFlavor (), view.getWorkbench ().getRegistry (), null));
//				view.dispose();
//				view3d.dispose();
//			}	
//		}, getContext());
//
//		ui.addComponent(containerRow, export, null);
		ui.addComponent(container, containerRow, null);
		if(this.sg instanceof SecGraph && ((SecGraph)this.sg).isRuleGraph()) {
			
			
			
			Object apply = ui.createButton("apply", 0, new Command() {

				@Override
				public String getCommandName() {
					// TODO Auto-generated method stub
					return "apply sg";
				}

				@Override
				public void run(Object info, Context context) {
					UI.executeLockedly(ctx.getWorkbench().getRegistry().getProjectGraph(), true, new Command() {

						@Override
						public String getCommandName() {
							// TODO Auto-generated method stub
							return null;
						}

						@Override
						public void run(Object info, Context context) {
							try {
//								SecGraphRef sgr = new SecGraphRef(sg.getName());
								((SecGraph)sg).execute();
								Runtime.INSTANCE.currentGraph().derive();
							} catch (NoRuleGraphException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
						}
						
					}, null, ctx, JobManager.ACTION_FLAGS);
				
					
					
				
					
				}}, getContext());
			ui.addComponent(containerRow, apply, null);
		}
		ComponentWrapperImpl dw = new ComponentWrapperImpl (container,null);
		return dw;
	}

	@Override
	public ComponentWrapper createPropertyEditorMenu() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transferable toTransferable(boolean includeChildren) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(boolean includeChildren) {
		
	}

	@Override
	public Context getContext() {
		return this.ctx;
	}
	public static void selectIt(Item item, Object info, Context ctx)
	{
		if (info instanceof ButtonWidget)
		{
			info = ((ButtonWidget) info).getProperty().getValue();
			if (info instanceof SecGraphImpl)
			{
				UIProperty.WORKBENCH_SELECTION.setValue (ctx, ((SecGraphImpl)info).toSelection(ctx));
			}
		}
	}
	
}
