package de.grogra.graph.object.impl.io;

import java.io.IOException;

import de.grogra.graph.impl.Node;
import de.grogra.graph.object.impl.GraphObjectImpl;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.ProjectFileObjectItem;
import de.grogra.util.MimeType;

public class GraphObjectFilter extends FilterBase implements ObjectSource {
	public static final IOFlavor FLAVOR = IOFlavor.valueOf (GraphObjectImpl.class);
	public GraphObjectFilter(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor (FLAVOR);
	}

	@Override
	public Object getObject() throws IOException {
		MimeType mt = source.getMetaData(ProjectFileObjectItem.FILE_MIMETYPE, 
				IO.getMimeType (source.getSystemId().toString ()));

		FileSource toRead = FileSource.createFileSource(getSystemId(), mt, getRegistry(), null);

		Object o = Workbench.current(). readObject (toRead, IOFlavor.NODE);

		if (o instanceof Node) {
			return new GraphObjectImpl( (Node) o , Workbench.current());
		}
		return null;

	}

}
